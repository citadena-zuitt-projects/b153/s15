function oddEvenChecker(num){
	if(typeof num === "number"){
		if(num % 2 === 0){
			console.log("The number is even.");
		} else {
				console.log("The number is odd.");
		}
	} else {
			alert("Invalid Input.");
	}
};

oddEvenChecker(143);

function budgetChecker(amount){
	if(typeof amount === "number"){
		if(amount > 40000){
			console.log("You are over the budget.");
		} else {
				console.log("You have resources left.");
		}
	} else {
			alert("Invalid Input.");
	}
};

budgetChecker(50000);